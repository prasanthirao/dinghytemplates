### Sample Dinghy Files


1. dinghyfileold - Sample Dinghy file with a single stage
2. dinghyfileold1 - Sample Dinghy file with 2 stages
3. dinghyfileold2 - Sample Dinghy file with Ref Ids (Stage Dependencies Example)
4. dinghyfileold3 - Sample Dinghy file with a module in the same location as dinghy file
    - wait.stage.module - Sample module file in the same location
5. dinghyfileold4 - Sample Dinghy file with a module in the sub folder(modules is the sub-folder here) of the same repo
    - modules/wait.stage.module1 - Sample module file in the sub-folder
6. dinghyfileold5 - Sample Dinghy file with a module in the sub folder(modules is the sub-folder here) of the same repo using variable substitution
    - modules/wait.stage.module2 - Sample module file in the sub-folder with variables and default values


